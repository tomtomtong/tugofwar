Meteor.methods(
{
	"updateLeftPower": function()
	{
		leftPower.update({'leftPower':{ $exists: true }}, {$inc:{'leftPower':1}});
	},
	"updateRightPower": function()
	{
		rightPower.update({'rightPower':{ $exists: true }}, {$inc: {'rightPower':1}});
	},
	"resetData":function()
	{
		leftPower.update({'leftPower':{ $exists: true }}, {$set:{'leftPower':0}});
		rightPower.update({'rightPower':{ $exists: true }}, {$set: {'rightPower':0}});
	},
	"insertStatus":function(side)
	{
		//console.log(status.findOne({name:Meteor.user().username}));
		if (status.findOne({'name':Meteor.user().username})==null)
		status.insert({'name':Meteor.user().username,'side':side});
		else
		{
			status.update({'name':Meteor.user().username},{$set:{'side':side}});
		}
	},
	"returnLeftPlayerNo":function()
	{
		var temp=status.find({side:"left"}).count();
		 return temp;
	},
	"returnRightPlayerNo":function()
	{
		var temp=status.find({side:"right"}).count();
		return temp;
	},
	"returnLeftPlayerName":function()
	{
		var names=[];
		var total=status.find({side:"left"}).count();
		for (var i=0; i<total; i++)
		{
			names.push(status.find({side:"left"}).fetch()[i]["name"]);
		}
		return names;
	},
	"returnRightPlayerName":function()
	{
		var names=[];
		var total=status.find({side:"right"}).count();
		for (var i=0; i<total; i++)
		{
			names.push(status.find({side:"right"}).fetch()[i]["name"]);
		}
		return names;
	}
}
)