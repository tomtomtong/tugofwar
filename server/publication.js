Meteor.publish('leftPower', function(username) {
  return leftPower.find({});
});

Meteor.publish('rightPower', function(username) {
  return rightPower.find({});
});

Meteor.publish('status', function(username) {

  // If for example we wanted to publish only logged in users we could apply:
  // filter = { userId: { $exists: true }};
 // var filter = {}; 
 return status.find({});
});