Template.gameScreen.onRendered(function(){

        canvas = document.getElementById('drawframe');
        paper.setup(canvas);
        group1 = new paper.Group(); //rope
        group2 = new paper.Group(); //node
        group3 = new paper.Group(); //player
        group4 = new paper.Group(); //timer

        raster1 = new paper.Raster();
        raster1.source = 'rope.jpg';
        raster1.scale(0.5); 
        raster1.position = paper.view.center;
        group1.addChild(raster1);

        var from = new paper.Point(paper.view.center.x, 50);
        var to = new paper.Point(paper.view.center.x, 200);
        middleLine = new paper.Path.Line(from, to);
        middleLine.strokeColor = 'black';
        middleLine.strokeWidth = 1;
        group1.addChild(middleLine);

        raster2 = new paper.Raster();
        raster2.source = 'node.png';
        raster2.scale(0.2);
        group2.addChild(raster2);

        //paper.view.onFrame=function(event){}
        setbutton();
        Meteor.subscribe("leftPower");
        Meteor.subscribe("rightPower",function(){
            Meteor.setInterval(redraw,100);
            });

   
     
        
        triggered=false;
        powerFactor=3;
        range=50;
        counter=10;
        Session.set("x",0);
        
        Meteor.setInterval(judge,100);
        Meteor.setInterval(setPlayerNames,100);
       // Meteor.call("resetData");
     
}
)

Template.gameScreen.events({
    "click #leftAddPower":function() {
             Meteor.call("updateLeftPower");
             redraw();
    },
    "click #rightAddPower":function() {
             Meteor.call("updateRightPower");
             redraw();
    }
})



resetCanvas=function()
{
    var myCircle = new paper.Path.Circle(paper.view.center, 1000);
    myCircle.fillColor = 'white'; 

}

redraw=function()
{	
    group2.removeChildren();
    //console.log($(window).width());

    var left=leftPower.findOne({'leftPower':{ $exists: true }});
    var right=rightPower.findOne({'rightPower':{ $exists: true }});
    
    var val=(right["rightPower"]-left["leftPower"])*powerFactor/2;
    
    var x=$(window).width()/2+val;

    var y=paper.view.center.y;

    Session.set("x",val);
    raster2.position = new paper.Point(x,y);
    group2.addChild(raster2);

    paper.view.draw();

}


setbutton=function()
{
    if ( Session.get("side")=="left")
        {
            $("#rightAddPower").hide();
        }
    else
        {
            $("#leftAddPower").hide();
        }
}

setLeftSidePlayer=function()
{
     group3.removeChildren();
    var text=[];
       
        for (var i=0; i<Session.get("leftPlayerName").length; i++)
        {
            text[i] = new paper.PointText({
            point: new paper.Point(i*100+100,40),
            justification: 'center',
            fontSize: 30,
            fillColor: 'black'
            });
            text[i].content=Session.get("leftPlayerName")[i];
            group3.addChild(text[i]);
        }
   
}

setRightSidePlayer=function()
{
   
    var text=[];
       
        for (var i=0; i<Session.get("rightPlayerName").length; i++)
        {
            text[i] = new paper.PointText({
            point: new paper.Point($(window).width()-150-i*100,40),
            justification: 'center',
            fontSize: 30,
            fillColor: 'black'
            });
            text[i].content=Session.get("rightPlayerName")[i];
            group3.addChild(text[i]);
        }
    paper.view.draw();
}

timer=function()
{

group4.removeChildren();
    if (counter>0){
    counter=counter-1;

        
         number = new paper.PointText({
            point: new paper.Point(paper.view.center.x,40),
            justification: 'center',
            fontSize: 30,
            fillColor: 'black'
            });
            number.content=counter;
        group4.addChild(number);
        paper.view.draw();
    }
    else
    {
        showWinner();
    }
  
    if (Math.abs(Session.get("x"))<=range)
    {
         Meteor.clearInterval(timerObject);    
         counter=10;
         triggered=false;
         group4.removeChildren();
    }

}
judge=function()
{
 //   console.log(Math.abs(Session.get("x")));
    if (Math.abs(Session.get("x"))>range && !triggered)
    {
            console.log("triggered");
            timerObject=Meteor.setInterval(timer,1500);
            triggered=true;
    }

}

showWinner=function(){

statement = new paper.PointText({
            point: new paper.Point(paper.view.center.x,40),
            justification: 'center',
            fontSize: 30,
            fillColor: 'black'
            });
        if (Session.get("x")>0)
        {
            statement.content="Right Side Wins";
        }
        else
        {
            statement.content="Left Side Wins";
        }
        group4.addChild(statement);
        paper.view.draw();

        setTimeout(function(){
        
         counter=10;
         triggered=false;
         Session.set("x",0);
         group4.removeChildren();
         Meteor.call("resetData");
         redraw();
         Meteor.clearInterval(timerObject);
       
        },5000);
  
}

setPlayerNames=function() {
         Meteor.call('returnLeftPlayerName',function(err,data){
         Session.set("leftPlayerName",data);
         });
         Meteor.call("returnRightPlayerName",function(err,data){
         Session.set("rightPlayerName",data);
         });
         Meteor.setTimeout(function(){
             setLeftSidePlayer();
            setRightSidePlayer(); 
        },1000)
}